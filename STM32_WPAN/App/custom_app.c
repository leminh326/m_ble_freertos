/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : App/custom_app.c
 * Description        : Custom Example Application (Server)
 ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_common.h"
#include "dbg_trace.h"
#include "ble.h"
#include "custom_app.h"
#include "custom_stm.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "cmsis_os.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
  /* Service1_L */
  uint8_t               Charac_s_2_Notification_Status;
  /* Service2_L */
  uint8_t               Charac_s_3_Notification_Status;
/* USER CODE BEGIN CUSTOM_APP_Context_t */
  uint8_t 				Custom_LED_Status;
/* USER CODE END CUSTOM_APP_Context_t */

  uint16_t              ConnectionHandle;
} Custom_App_Context_t;

/* USER CODE BEGIN PTD */
osThreadId_t ControlResetThreadId;
/* USER CODE END PTD */

/* Private defines ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macros -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/**
 * START of Section BLE_APP_CONTEXT
 */

PLACE_IN_SECTION("BLE_APP_CONTEXT") static Custom_App_Context_t Custom_App_Context;

/**
 * END of Section BLE_APP_CONTEXT
 */

/* USER CODE BEGIN PV */
uint8_t UpdateCharData[247];
uint8_t NotifyCharData[247];

uint8_t SecureReadData;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
  /* Service1_L */
static void Custom_Charac_s_2_Update_Char(void);
static void Custom_Charac_s_2_Send_Notification(void);
  /* Service2_L */
static void Custom_Charac_s_3_Update_Char(void);
static void Custom_Charac_s_3_Send_Notification(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Functions Definition ------------------------------------------------------*/
void Custom_STM_App_Notification(Custom_STM_App_Notification_evt_t *pNotification)
{
/* USER CODE BEGIN CUSTOM_STM_App_Notification_1 */

/* USER CODE END CUSTOM_STM_App_Notification_1 */
  switch(pNotification->Custom_Evt_Opcode)
  {
/* USER CODE BEGIN CUSTOM_STM_App_Notification_Custom_Evt_Opcode */
#if(BLE_CFG_OTA_REBOOT_CHAR != 0)
    case CUSTOM_STM_BOOT_REQUEST_EVT:
      APP_DBG_MSG("-- P2P APPLICATION SERVER : BOOT REQUESTED\n");
      APP_DBG_MSG(" \n\r");

      *(uint32_t*)SRAM1_BASE = *(uint32_t*)pNotification->DataTransfered.pPayload;
      NVIC_SystemReset();
      break;
#endif
/* USER CODE END CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

  /* Service1_L */
    case CUSTOM_STM_CHARAC_S_1_READ_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_1_READ_EVT */

/* USER CODE END CUSTOM_STM_CHARAC_S_1_READ_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_1_WRITE_NO_RESP_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_1_WRITE_NO_RESP_EVT */
        if(pNotification->DataTransfered.pPayload[0] == 0x00){ /* ALL Deviceselected - may be necessary as LB Routeur informs all connection */
          if(pNotification->DataTransfered.pPayload[1] == 0x01)
          {
        	APP_DBG_MSG(" LED ON \n");
        	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
        	Custom_App_Context.Custom_LED_Status = 0x01;
          }
          if(pNotification->DataTransfered.pPayload[1] == 0x00)
          {
        	APP_DBG_MSG(" LED OFF \n");
        	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
        	Custom_App_Context.Custom_LED_Status = 0x00;
          }
        }
/* USER CODE END CUSTOM_STM_CHARAC_S_1_WRITE_NO_RESP_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_2_READ_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_2_READ_EVT */

/* USER CODE END CUSTOM_STM_CHARAC_S_2_READ_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_2_NOTIFY_ENABLED_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_2_NOTIFY_ENABLED_EVT */
        PRINT_MESG_DBG("NOTIFICATION_ENABLED\n");
        Custom_App_Context.Charac_s_2_Notification_Status = 1;
        Control_Reset_Task_Init();

/* USER CODE END CUSTOM_STM_CHARAC_S_2_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_2_NOTIFY_DISABLED_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_2_NOTIFY_DISABLED_EVT */
        PRINT_MESG_DBG("NOTIFICATION_DISABLED\n");
        Custom_App_Context.Charac_s_2_Notification_Status = 0;
        osThreadTerminate (ControlResetThreadId);
/* USER CODE END CUSTOM_STM_CHARAC_S_2_NOTIFY_DISABLED_EVT */
      break;

  /* Service2_L */
    case CUSTOM_STM_CHARAC_S_3_READ_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_3_READ_EVT */

/* USER CODE END CUSTOM_STM_CHARAC_S_3_READ_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_3_NOTIFY_ENABLED_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_3_NOTIFY_ENABLED_EVT */

/* USER CODE END CUSTOM_STM_CHARAC_S_3_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_CHARAC_S_3_NOTIFY_DISABLED_EVT:
/* USER CODE BEGIN CUSTOM_STM_CHARAC_S_3_NOTIFY_DISABLED_EVT */

/* USER CODE END CUSTOM_STM_CHARAC_S_3_NOTIFY_DISABLED_EVT */
      break;

    default:
/* USER CODE BEGIN CUSTOM_STM_App_Notification_default */

/* USER CODE END CUSTOM_STM_App_Notification_default */
      break;
  }
/* USER CODE BEGIN CUSTOM_STM_App_Notification_2 */

/* USER CODE END CUSTOM_STM_App_Notification_2 */
  return;
}

void Custom_APP_Notification(Custom_App_ConnHandle_Not_evt_t *pNotification)
{
/* USER CODE BEGIN CUSTOM_APP_Notification_1 */

/* USER CODE END CUSTOM_APP_Notification_1 */

  switch(pNotification->Custom_Evt_Opcode)
  {
/* USER CODE BEGIN CUSTOM_APP_Notification_Custom_Evt_Opcode */

/* USER CODE END P2PS_CUSTOM_Notification_Custom_Evt_Opcode */
  case CUSTOM_CONN_HANDLE_EVT :
/* USER CODE BEGIN CUSTOM_CONN_HANDLE_EVT */

/* USER CODE END CUSTOM_CONN_HANDLE_EVT */
    break;

    case CUSTOM_DISCON_HANDLE_EVT :
/* USER CODE BEGIN CUSTOM_DISCON_HANDLE_EVT */

/* USER CODE END CUSTOM_DISCON_HANDLE_EVT */
    break;

    default:
/* USER CODE BEGIN CUSTOM_APP_Notification_default */

/* USER CODE END CUSTOM_APP_Notification_default */
      break;
  }

/* USER CODE BEGIN CUSTOM_APP_Notification_2 */

/* USER CODE END CUSTOM_APP_Notification_2 */

  return;
}

void Custom_APP_Init(void)
{
/* USER CODE BEGIN CUSTOM_APP_Init */

/* USER CODE END CUSTOM_APP_Init */
  return;
}

/* USER CODE BEGIN FD */

/* USER CODE END FD */

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/

  /* Service1_L */
void Custom_Charac_s_2_Update_Char(void) /* Property Read */
{
  Custom_STM_App_Update_Char(CUSTOM_STM_CHARAC_S_2, (uint8_t *)UpdateCharData);
  /* USER CODE BEGIN Charac_s_2_UC*/

  /* USER CODE END Charac_s_2_UC*/
  return;
}

void Custom_Charac_s_2_Send_Notification(void) /* Property Notification */
 {
  if(Custom_App_Context.Charac_s_2_Notification_Status)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_CHARAC_S_2, (uint8_t *)NotifyCharData);
    /* USER CODE BEGIN Charac_s_2_NS*/

    /* USER CODE END Charac_s_2_NS*/
  }
  else
  {
    APP_DBG_MSG("-- CUSTOM APPLICATION : CAN'T INFORM CLIENT -  NOTIFICATION DISABLED\n ");
  }
  return;
}

  /* Service2_L */
void Custom_Charac_s_3_Update_Char(void) /* Property Read */
{
  Custom_STM_App_Update_Char(CUSTOM_STM_CHARAC_S_3, (uint8_t *)UpdateCharData);
  /* USER CODE BEGIN Charac_s_3_UC*/

  /* USER CODE END Charac_s_3_UC*/
  return;
}

void Custom_Charac_s_3_Send_Notification(void) /* Property Notification */
 {
  if(Custom_App_Context.Charac_s_3_Notification_Status)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_CHARAC_S_3, (uint8_t *)NotifyCharData);
    /* USER CODE BEGIN Charac_s_3_NS*/

    /* USER CODE END Charac_s_3_NS*/
  }
  else
  {
    APP_DBG_MSG("-- CUSTOM APPLICATION : CAN'T INFORM CLIENT -  NOTIFICATION DISABLED\n ");
  }
  return;
}

/* USER CODE BEGIN FD_LOCAL_FUNCTIONS*/
void Control_Reset_Task(void *argument)
{
	while(1)
	{
		osDelay(1000);
		UpdateCharData[0] = UpdateCharData[0] +1;
		UpdateCharData[1] = UpdateCharData[1] +2;
		Custom_STM_App_Update_Char(CUSTOM_STM_CHARAC_S_1, (uint8_t *)UpdateCharData);

        NotifyCharData[0] = NotifyCharData[0] + 1;
        Custom_App_Context.Charac_s_2_Notification_Status = 1;
        Custom_Charac_s_2_Send_Notification();
        printf("Control_Reset_Task \n");
	}
}

void Control_Reset_Task_Init()
{
	const osThreadAttr_t ControlResetTask_attributes =
	{
		  .name = "ControlResetTask",
		  .priority = (osPriority_t) osPriorityHigh7,
		  .stack_size = 128*5
	};
	ControlResetThreadId = osThreadNew(Control_Reset_Task, NULL, &ControlResetTask_attributes);
}
/* USER CODE END FD_LOCAL_FUNCTIONS*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
